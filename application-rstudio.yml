---
- name: Install and configure r studio
  hosts:
    - localhost
  gather_facts: false
  vars_files: vars/version.yml
  become: true
  tasks:
    - name: Wait for system to become reachable
      wait_for_connection:
        timeout: 300

    - name: Gather facts for first time
      setup:

    - name: Ubuntu
      when: ansible_distribution == 'Ubuntu'
      block:
        - name: Install required packages
          package:
            name:
              - apt-transport-https
              - dpkg-sig
              - gdebi-core
              - libxml2-dev
              - libcurl4-openssl-dev
              - libssl-dev
              - git
            state: present

        - name: Install tidy-verse packages
          package:
            name:
              - libicu-dev
              - make
              - libcurl4-openssl-dev
              - libssl-dev
              - zlib1g-dev
              - libfontconfig1-dev
              - libfreetype6-dev
              - libfribidi-dev
              - libharfbuzz-dev
              - libjpeg-dev
              - libpng-dev
              - libtiff-dev
              - pandoc
              - libxml2-dev
            state: present

        - name: Add an apt key by id from a keyserver
          apt_key:
            keyserver: keyserver.ubuntu.com
            id: E298A3A825C0D65DFD57CBB651716619E084DAB9

        - name: Download cmake installer script
          get_url:
            url: "{{cmake.url}}/{{cmake.script}}"
            dest: ./

        - name: Create cmake directory
          become: true
          file:
            path: /etc/src-cmake
            state: directory
            mode: '0755'

        - name: Run cmake install script
          command: >
            sudo
            bash
            ./{{cmake.script}}
            --skip-license
            --prefix=/etc/src-cmake
            --exclude-subdir

        - name: Create symbolic link to cmake
          become: true
          shell: ln -s /etc/src-cmake/bin/cmake /usr/bin/cmake

        - name: Ensure R-Studio repository is present
          apt_repository:
            repo: "{{ available_versions[r_version]['repo'] }}"
            state: present

        - name: Ensure r-base and r-base-dev are installed
          package:
            name:
              - r-recommended={{ available_versions[r_version]['package'] }}
              - r-base={{ available_versions[r_version]['package'] }}
              - r-base-dev={{ available_versions[r_version]['package'] }}
            state: present

        - name: Ensure R-Studio server is installed
          apt:
            deb: "{{ available_versions[r_version]['deb'] }}"
            allow_unauthenticated: true

        - name: Create nginx location block
          copy:
            src: r-studio.conf
            dest: /etc/nginx/app-location-conf.d/r-studio.conf
            mode: 0644

        - name: Add repo packagemanager in Rprofile.site
          lineinfile:
            path: /usr/lib/R/etc/Rprofile.site
            regexp: '^#  options\(defaultPackages*'
            insertafter: '#  options\(defaultPackages = c\(old, "MASS"\), repos = r\)'
            line: options(repos = c(CRAN = "{{ available_versions[r_version]['cran_url'] }}"))

        - name: Switch off session timeout and suspending to disk
          copy:
            content: |
              # R Session Configuration File
              session-timeout-minutes=0
              session-timeout-suspend=0

            dest: /etc/rstudio/rsession.conf
            mode: 0644

        - name: Give all workspace users rw access to generated files
          lineinfile:
            path: /usr/lib/R/etc/Rprofile.site
            line: "Sys.umask('000')"

        - name: Install custom CRAN packages
          when: cran_packages | trim | length > 0
          command: >
            R --slave -e '
            install.packages(
            pkgs=c(
            {{(cran_packages | split | to_json)[1:-1]}}),
            lib="/usr/local/lib/R/site-library",
            dependencies=TRUE
            )'
          register: install_pckgs_command
  
        # - name: log install output
        #   debug:
        #     var: install_pckgs_command
  
        - name: Restart nginx
          service:
            name: nginx
            state: restarted

        - name: Start rstudio
          register: start_rstudio
          service:
            name: rstudio-server
            state: started
        
        - name: log rstudio start output   
          debug:
            msg: 'service_url: {"url": "https://{{ ansible_host }}", "tag": "web", "description": "R-Studio"}'
          when: (start_rstudio is succeeded)
